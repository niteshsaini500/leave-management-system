<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('users', 'UserController@store');
Route::post('login', 'AuthController@login');

Route::group([
 
   'middleware' => ['api','jwt.verify'],
 
], function ($router) {
 	
	Route::get('users', 'UserController@index');	

  Route::post('logout', 'AuthController@logout');
  Route::post('refresh', 'AuthController@refresh');
  Route::post('me', 'AuthController@me');


  Route::group(['namespace' => 'Leave'], function() {
    Route::group(['prefix'=>'leave'], function() {

      Route::post('request', 'RequestController@requestLeave');
      Route::get('balance', 'BalanceController@requestBalance');
      // administrator route
      Route::get('request/list', 'RequestController@index');
      Route::post('request/confirmation', 'RequestController@confirmation');
      Route::group(['prefix'=>'get_user'], function() {
        Route::post('balance', 'BalanceController@userBalance');
        Route::post('leave/status', 'RequestController@leaveStatus');
      });

    });
});



});