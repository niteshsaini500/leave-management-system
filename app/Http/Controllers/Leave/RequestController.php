<?php

namespace App\Http\Controllers\Leave;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\LeaveLog; 
use App\Models\Leave; 
use App\Models\Year; 

class RequestController extends Controller
{
	public function index(Request $request){

    	$user = Auth::user();
    	if ($user->user_type != 'admin') {
    		return response()->json(array('status'=>'error', 'message'=>'Unauthorized Acess.'), 403);
    	}

    	$filters = [];
    	$filters['status'] = 'pending';
    	$leave_request_list = LeaveLog::getList($filters);

    	return $leave_request_list;
    }

    public function requestLeave(Request $request){

    	$input = $request->all();
		$request->validate([
			'reason'			=>	'required',
		]);

    	$user = Auth::user();
	    $current_year = date('Y');
		$year = Year::where('year', $current_year)->first();
		if (is_null($year)) {
     	 	return response()->json(array('status'=>'error', 'message'=>'Year not found.'), 404);
       	}

		$leaves = Leave::where('user_id', $user->id)->first();
		if (is_null($leaves)) {
			return response()->json(array('status'=>'error', 'message'=>'Data not found.'), 404);
		}
		if ($leaves->current_balance <= 0) {
       		return response()->json(array('status'=>'success', 'message'=>"You don't have leave balance in your account."), 200);
		}

       \DB::beginTransaction();

		$check_leave = LeaveLog::where('user_id', $user->id)
								->where('year_id', $year->id)
								->where('date', date('Y-m-d'))
								->where('status', 'pending')
								->first();
		
		if (!is_null($check_leave)) {
	   		return response()->json(array('status'=>'success', 'message'=>"Request already raised."), 200);
		}	


		$leave = new LeaveLog;
		$leave->user_id = $user->id;
		$leave->year_id = $year->id;
		$leave->date = date('Y-m-d');
		$leave->reason = isset($input['reason']) && $input['reason'] ? $input['reason'] : '';
		$leave->status = 'pending';
		$leave->save();	

		$leaves->current_balance = ($leaves->current_balance-1);
		$leaves->save();

       \DB::commit();

       return response()->json(array('status'=>'success', 'message'=>'Leave request created successfully.'), 200);

    }

    public function confirmation(Request $request){
    	
    	$input = $request->all();
		$request->validate([
			'leave_log_id'			=>	'required',
			'user_id'				=>	'required',
			'status'				=>	'required',
		]);

		$leave_log = LeaveLog::where('id', $input['leave_log_id'])->where('user_id', $input['user_id'])->first();
		if (is_null($leave_log)) {
			return response()->json(array('status'=>'error', 'message'=>'Leave request not found.'), 404);
		}

		\DB::beginTransaction();

		$leave_log->status = $input['status'];
		$leave_log->save();

		if ($leave_log->status != 'approve') {
			$leaves = Leave::where('user_id', $input['user_id'])->first();
			$leaves->current_balance = ($leaves->current_balance+1);
			$leaves->save();
		}

       \DB::commit();

       return response()->json(array('status'=>'success', 'message'=>'Leave request status updated.'), 200);

       return $leave;
    }


    public function leaveStatus(Request $request){

    	$input = $request->all();
		$request->validate([
			'leave_log_id'			=>	'required',
			'user_id'				=>	'required',
		]);

		$user = Auth::user();
    	if ($user->user_type != 'admin') {
    		return response()->json(array('status'=>'error', 'message'=>'Unauthorized Acess.'), 403);
    	}

		$leave_log = LeaveLog::where('user_id', $input['user_id'])->where('id', $input['leave_log_id'])->first();
		if (is_null($leave_log)) {
			return response()->json(array('status'=>'error', 'message'=>'Data not found.'), 403);
		}

       return response()->json(array('status'=>'success', 'message'=>"Leave status ". $leave_log->status . "."), 200);

    }

}
