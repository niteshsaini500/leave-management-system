<?php

namespace App\Http\Controllers\Leave;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\LeaveLog; 
use App\Models\Leave; 
use App\Models\Year; 

class BalanceController extends Controller
{
    public function requestBalance(Request $request){

    	$user = Auth::user();
	    $current_year = date('Y');
  		$year = Year::where('year', $current_year)->first();
  		if (is_null($year)) {
       	 	return response()->json(array('status'=>'error', 'message'=>'Year not found.'), 200);
       	}

  		$leaves = Leave::where('user_id', $user->id)->first();
  		if (is_null($leaves)) {
     			return response()->json(array('status'=>'error', 'message'=>"Data not found!"), 404);
  		}
     		return response()->json(array('status'=>'success', 'message'=>"You have " . $leaves->current_balance . " leave balance in your account."), 200);

      }

    public function userBalance(Request $request){

      $input = $request->all();
      $request->validate([
        'user_id'      =>  'required',
      ]);

      $user = Auth::user();
      if ($user->user_type != 'admin') {
        return response()->json(array('status'=>'error', 'message'=>'Unauthorized Acess.'), 403);
      }

      
      $leaves = Leave::where('user_id', $input['user_id'])->first();
      if (is_null($leaves)) {
          return response()->json(array('status'=>'error', 'message'=>"Data not found!"), 404);
      }
        return response()->json(array('status'=>'success', 'message'=>"User have " . $leaves->current_balance . " leave balance in their account."), 200);

      }
        
}
