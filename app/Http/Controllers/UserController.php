<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Leave; 
use App\Models\Year; 
use App\User;
 
class UserController extends Controller
{
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
      $user = Auth::user();
      if ($user->user_type != 'admin') {
        return response()->json(array('status'=>'error', 'message'=>'Unauthorized Acess.'), 403);
      }        
      $users = User::all();
 
      return $users;
   }
 
   /**
    * Store a newly created resource in storage.
    *
    */
   public function store(Request $request)
   {
       
       $input = $request->all();
       $user = User::where('email', $input['email'])->first();
       if (!is_null($user)) {
         return response()->json(array('status'=>'error', 'message'=>'User Already Exist.'), 404);
       }

       $userData = $request->all();
       $user = User::create($userData);

       $current_year = date('Y');
       $year = Year::where('year', $current_year)->first();
       if (is_null($year)) {
          return response()->json(array('status'=>'error', 'message'=>'Year not found.'), 404);
         
       }
       \DB::beginTransaction();
       
       $leave = new Leave;
       $leave->user_id = $user->id;
       $leave->year_id = $year->id;
       $leave->total_leaves = 24;
       $leave->current_balance = 24;
       $leave->save();

       \DB::commit();

       return response()->json(array('status'=>'success', 'message'=>'User created successfully. Total 24 Leave credited in your account.'), 200);

       // return $user;
   }
}