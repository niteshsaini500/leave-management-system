<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class LeaveLog extends Model
{
    protected $table = 'leave_logs';
    protected $fillable = ['user_id', 'year_id', 'date'];

    public static function getList($filters = []) {

		$query = DB::table('leave_logs')
					            ->join('users', 'leave_logs.user_id', '=', 'users.id')
					            ->join('years', 'leave_logs.year_id', '=', 'years.id')
								->select('leave_logs.id', 'leave_logs.date', 'leave_logs.reason', 'leave_logs.status', 'users.name', 'users.email', 'years.year')
								->where('leave_logs.flag', 1);
		
		if (isset($filters['status']) && $filters['status']) {
			$query = $query->where('leave_logs.status', $filters['status']);
		}								

		$result = $query->get();
		return $result;
	}
}
