<?php

use Illuminate\Database\Seeder;

class YearsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=2001; $i < 2050; $i++) { 
        	DB::table('years')->insert([
	            'year' => $i,
	            'created_at' => NOW(),
	            'updated_at' => NOW(),
	        ]);
        }
    }
}
